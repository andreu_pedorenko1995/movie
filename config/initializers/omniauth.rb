Rails.application.config.middleware.use OmniAuth::Builder do
  provider :vkontakte, ENV['vk_key'], ENV['vk_secret'], scope: 'email'
  provider :facebook, ENV['FB_KEY'], ENV['FB_SECRET'], {scope: 'email'}
end