Rails.application.routes.draw do
  get 'log_out', to: 'sessions#destroy', as: 'log_out'
  get 'log_in', to: 'sessions#new', as: 'log_in' 
  get 'sign_up' , to: 'users#new', as: 'sign_up'
 
  resources :users do
    get :change_permission, on: :member
  end
  
  resources :sessions
  get 'users/new'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'welcome#index'

  get 'auth/vkontakte/callback', to: 'omniauth#create'
  get 'auth/facebook/callback', to: 'omniauth#create'

  namespace :admin do
    root 'users#index'
    resources :users, only: [:index, :destroy]
    resources :videos
    resources :films
    resources :cartoons
  end

  resources :videos do
     member do
      put "like", to: "videos#upvote"
      put "dislike", to: "videos#downvote"
    end
    get :favourite, on: :member
    get '/videos', to: 'pages#video'
  end

  resources :films do
    resources :comments
    member do
      put "like", to: "films#upvote"
      put "dislike", to: "films#downvote"
    end
    get '/films', to: 'pages#film'
  end

  resources :cartoons do
    member do
      put "like", to: "cartoons#upvote"
      put "dislike", to: "cartoons#downvote"
    end
    get '/cartoons', to: 'pages#cartoon'
  end
end
