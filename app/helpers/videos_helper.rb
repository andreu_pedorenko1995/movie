module VideosHelper

  def favourite_video(video)
    if current_user && current_user.favourite_videos.map(&:video).include?(video)
    'text-danger'
    else
    'text-muted'
    end
  end
end
