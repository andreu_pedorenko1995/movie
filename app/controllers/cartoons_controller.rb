class CartoonsController < ApplicationController
    
  def index
    @cartoons = Cartoon.all.page(params[:page]).where("name LIKE ?", "%#{params[:search]}%") 
  end

  def upvote 
    @cartoon = Cartoon.find(params[:id])
    @cartoon.upvote_by current_user
    redirect_to :back
  end  

  def downvote
    @cartoon = Cartoon.find(params[:id])
    @cartoon.downvote_by current_user
    redirect_to :back
  end

  private


  def cartoon_params
    params.require(:cartoon).permit(:name)
  end
end
