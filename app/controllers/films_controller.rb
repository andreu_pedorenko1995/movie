class FilmsController < ApplicationController
	def index
    @films = Film.all.page(params[:page]).where("name LIKE ?", "%#{params[:search]}%")
  end

  def show
  	@film = Film.find(params[:id])
  end

  def upvote 
    @film = Film.find(params[:id])
    @film.upvote_by current_user
    redirect_to :back
  end  

  def downvote
    @film = Film.find(params[:id])
    @film.downvote_by current_user
    redirect_to :back
  end

  private

  def film_params 
    params.require(:film).permit(:name, :quality, :genre, :year, :country, :duration, :producer, :premiere, :transfer, :image_film, :description, :screenshots_and_actors, :link, :treilerlink, category_ids: [])
  end
end
