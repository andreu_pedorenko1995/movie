class Admin::VideosController < AdminsController
  before_action :authorize, only: [:index, :destroy, :new]
  before_action :require_admin, only: [:index, :destroy, :new]
  before_action :current_video, only: [:show, :edit, :update, :destroy]


  def index
    @videos = Video.all.where("name LIKE ?", "%#{params[:search]}%")
  end

  def new
    @video = Video.new
  end

  def create
    
  end

  def create
      @video = Video.create(video_params)
    if @video.save
      redirect_to admin_videos_path 
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @video.update(video_params)
      redirect_to admin_videos_path 
    else
      render 'edit'
    end
  end

  def destroy
    @video.destroy
    redirect_to admin_videos_path 
  end


  private

  def video_params 
    params.require(:video).permit(:name, :link)
  end

  def current_video
    @video = Video.find(params[:id])
  end

  def require_admin
    redirect_to root_path unless current_user.try(:admin?)
    flash[:success] = "You admin?"
  end
end