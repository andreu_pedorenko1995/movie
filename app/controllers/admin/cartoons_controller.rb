class Admin::CartoonsController < AdminsController
  before_action :authorize, only: [:index, :destroy, :new]
  before_action :require_admin, only: [:index, :destroy, :new]
  before_action :current_cartoon, only: [:show, :edit, :update, :destroy]


  def index
    @cartoons = Cartoon.all.where("name LIKE ?", "%#{params[:search]}%")
  end

  def new
    @cartoon = Cartoon.new
  end

  def create
    @cartoon = Cartoon.create(cartoon_params)
    if @cartoon.save
      redirect_to admin_cartoons_path 
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @cartoon.update(cartoon_params)
      redirect_to admin_cartoon_path 
    else
      render 'edit'
    end
  end

  def destroy
    @cartoon.destroy
    redirect_to admin_cartoons_path 
  end


  private

  def cartoon_params 
    params.require(:cartoon).permit(:name, :link)
  end

  def current_cartoon
    @cartoon = Cartoon.find(params[:id])
  end

  def require_admin
    redirect_to root_path unless current_user.try(:admin?)
    flash[:success] = "You admin?"
  end
end