class Admin::UsersController < AdminsController
  before_action :authorize, only: [:index, :destroy]
  before_action :require_admin, only: [:index, :destroy]

  def index
    @users = User.all 
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
  end

  private

  def require_admin
    redirect_to root_path unless current_user.try(:admin?)
    flash[:success] = "You admin?"
  end
end