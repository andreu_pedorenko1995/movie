class Admin::FilmsController < AdminsController
  before_action :authorize, only: [:index, :destroy, :new]
  before_action :require_admin, only: [:index, :destroy, :new]
  before_action :current_film, only: [:show, :edit, :update, :destroy]

  def index
    @films = Film.all.where("name LIKE ?", "%#{params[:search]}%")
  end

  def new
    @film = Film.new
  end

  def create
    @film = Film.create(film_params)
    if @film.save
      redirect_to admin_films_path 
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @film.update(film_params)
      redirect_to admin_films_path 
    else
      render 'edit'
    end
  end

  def destroy
    @film.destroy
    redirect_to admin_films_path 
  end


  private

  def film_params 
    params.require(:film).permit(:name, :quality, :genre, :year, :country, :duration, :producer, :premiere, :transfer, :image_film, :description, :screenshots_and_actors, :link, :treilerlink, category_ids: [])
  end

  def current_film
    @film = Film.find(params[:id])
  end

  def require_admin
    redirect_to root_path unless current_user.try(:admin?)
    flash[:success] = "You admin?"
  end
end