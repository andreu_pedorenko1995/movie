class VideosController < ApplicationController
  before_action :current_video, only: [:favourite]
    
  def index
    @videos = Video.all.page(params[:page]).where("name LIKE ?", "%#{params[:search]}%")
    
  end

  def favourite
    current_user.favourite_videos.manage(@video)
  end

  def upvote 
    @video = Video.find(params[:id])
    @video.upvote_by current_user
    redirect_to :back
  end  

  def downvote
    @video = Video.find(params[:id])
    @video.downvote_by current_user
    redirect_to :back
  end

  private

  def current_video
    @video = Video.find(params[:id])
  end

  def video_params
    params.require(:video).permit(:name)
  end

end
