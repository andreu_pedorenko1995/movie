class Film < ApplicationRecord
  dragonfly_accessor :imagefilm
  dragonfly_accessor :screenshotsactors
  has_and_belongs_to_many :categories
  belongs_to :category

  acts_as_votable
  has_many :comments

  self.per_page = 5
end
