class FavouriteVideo < ApplicationRecord
  belongs_to :user
  belongs_to :video

  def self.manage(video)
    object = find_by(video: video)
    object ? object.destroy : create(video: video)
  end
end
