class CreateCategoriesFilms < ActiveRecord::Migration[5.0]
  def change
  	create_table :categories_films, id: false do |t|
      t.belongs_to :category, index: true
      t.belongs_to :film, index: true
    end
  end
end
