class CreateFavouriteVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :favourite_videos do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :video, foreign_key: true

      t.timestamps
    end
  end
end
