class AddTreilerlinkToFilms < ActiveRecord::Migration[5.0]
  def change
    add_column :films, :treilerlink, :string
  end
end
