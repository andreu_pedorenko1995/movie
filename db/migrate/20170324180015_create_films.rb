class CreateFilms < ActiveRecord::Migration[5.0]
  def change
    create_table :films do |t|
      t.string :name
      t.string :quality
      t.string :genre
      t.date   :year
      t.string :country
      t.string :duration
      t.string :producer
      t.string :premiere
      t.string :transfer
      t.string :imagefilm_uid
      t.string :description
      t.string :screenshotsactors_uid
      t.string :link

      t.timestamps
    end
  end
end
