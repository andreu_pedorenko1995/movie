# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Category.create(name: '2017 года')
Category.create(name: '2016 года')
Category.create(name: 'Русские')
Category.create(name: 'Зарубежные')
Category.create(name: 'Драмы')
Category.create(name: 'Детективы')
Category.create(name: 'Мелодрамы')
Category.create(name: 'Военные')
Category.create(name: 'Боевики')
Category.create(name: 'Комедии')
Category.create(name: 'Триллеры')
Category.create(name: 'Ужасы')